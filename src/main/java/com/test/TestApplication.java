package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class TestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestApplication.class, args);
	}

	@GetMapping("")
	public String test() {
		return "update success!!";
	}

	@GetMapping("/test2")
	public String test2() {
		return "test 2 success!!";
	}

	@GetMapping("/main")
	public String main() {
		return "main branch success!!";
	}

	@GetMapping("/dev-2")
	public String dev2() {
		return "dev2 success!!";
	}

	@GetMapping("/dev1")
	public String dev1() {
		return "dev1 success!!";
	}

	@GetMapping("/dev2")
	public String dev_2() {
		return "dev2 success!!";
	}
	
}
